
import Tkinter as Tk
import pong_events as p_e

"""this code sets up the basic frame of the code including
the  canvas, ball, paddle, scores and buttons"""

root = Tk.Tk()

bkimage = Tk.PhotoImage(file="resources/bkg.gif")
w = bkimage.width()
h = bkimage.height()

ballimg = Tk.PhotoImage(file="resources/ball.gif")
ballw = ballimg.width()
ballh = ballimg.height()

paddle1img = Tk.PhotoImage(file="resources/paddle/w11.gif")
paddle2img = Tk.PhotoImage(file="resources/paddle/w21.gif")
paddlew = paddle1img.width()
paddleh = paddle1img.height()

chaotic_cloud_img = Tk.PhotoImage(file="resources/chaotic_cloud.gif")
creature_img = Tk.PhotoImage(file="resources/creature.gif")

class Application(Tk.Frame):

	def __init__(self, master=None):
		self.master_ = master
		Tk.Frame.__init__(self, self.master_)

		self.frame1=[]
		self.frame2=[]

		root.resizable(width=False, height=False)
		root.title('Pong!')
		root.geometry("%dx%d+0+0" % (w, h))
		self.canvas = Tk.Canvas(root, width=w, height=h)
		self.canvas.pack(side='top', fill='both', expand='yes')
		self.canvas.create_image(0, 0, image=bkimage, anchor='nw')
		self.ball = self.canvas.create_image((w - ballw)/2.0 , (h - ballh)/2.0, image=ballimg, anchor='nw')

		self.paddleone = self.canvas.create_image(0, (h - paddleh)/2.0, image=paddle1img, anchor='nw')
		self.paddletwo = self.canvas.create_image(w, (h - paddleh)/2.0, image=paddle2img, anchor='ne')

		scorecolor = '#%02x%02x%02x' % (0, 220, 251)
		self.scoreone = self.canvas.create_text(w/2.0 - 20,0, text = '0',anchor='ne', fill = "red", font =("Helvetica", 30))
		self.scoretwo = self.canvas.create_text(w/2.0 + 20,0, text = '0',anchor='nw', fill = scorecolor, font =("Helvetica", 30))

		for i in range(16):

			fname1="resources/paddle/w1"+str(i+1)+".gif"
			self.frame1+=[Tk.PhotoImage(file=fname1)]

			fname2="resources/paddle/w2"+str(i+1)+".gif"
			self.frame2+=[Tk.PhotoImage(file=fname2)]

		self.pong = p_e.Pong(self.master_,self.canvas,self.ball,
				[self.paddleone,self.paddletwo,self.frame1,self.frame2],
				[self.scoreone,self.scoretwo],chaotic_cloud_img,creature_img)

	def runit(self):
		self.pong.run()
		self.master_.after(5,self.runit)

	def restart(self):
		Application(master=root) # restart application

	def quit(self):
		self.pong.do = 0 # stop animation
		self.master.destroy() # close window

app = Application(root).runit()
root.mainloop()

