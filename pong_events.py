""" This code contains all of the main events of pong GUI"""
import numpy as np
import random as rd
import obstacles as obs
import gravity_field as gf
import portal as po

def dot(x, y):
	"""
	2D dot product
	"""
	return x[0]*y[0] + x[1]*y[1]

def scale(x, a):
	"""
	2D scalar multiplication
	"""
	return (x[0]*a, x[1]*a)

def add(x, y):
	"""
	2D vector addition
	"""
	return (x[0]+y[0], x[1]+y[1])

class Ball(object):
	def __init__(self, _canvas,_ball):
		"""
		Create a pong ball with the given inital position. 
		"""
		self.balldiameter = 40
		self.position = (_canvas.coords(_ball)[0] + self.balldiameter/2.0,
		_canvas.coords(_ball)[1] + self.balldiameter/2.0)
		# center of gravity of ball
		
		self.velocity = (rd.choice([-1,1])*4,rd.choice([-1,1])*4)

		self.canvas_ = _canvas
		self.ball_ = _ball
		self.Nhits = 0

	def random_vel(self):

		self.velocity = (rd.choice([-1,1])*self.velocity[0]
		,rd.choice([-1,1])*self.velocity[1])

	def reflect(self, surface, opt):
		"""
		Alter the ball's velocity for a perfectly elastic
		collision with a surface defined by the unit normal surface.
		
		surface: a tuple of floats
		"""
		diagonal = -2 * dot(surface, self.velocity)
		self.velocity = add(self.velocity, scale(surface, diagonal))

		if (opt == 'paddle'):
			
			self.Nhits = self.Nhits + 1

			if (surface[0] == -1):

				self.position = [1100 - 30 - self.balldiameter/2.0
				, self.position[1]]

			elif (surface[0] == 1):

				self.position = [30 + self.balldiameter/2.0, self.position[1]]

			self.canvas_.coords(self.ball_,self.position[0] - self.balldiameter/2.0 ,
			self.position[1] - self.balldiameter/2.0)

			if (self.Nhits % 5 == 0 and self.Nhits != 0):

			# increase ball velocity by 20 percent every five hits

				self.velocity = scale(self.velocity,1.2) 

	def hits_portal(self,teleportto):

		self.position = teleportto
		self.canvas_.coords(self.ball_,self.position[0] - self.balldiameter/2.0 ,
		self.position[1] - self.balldiameter/2.0)

	def move(self):
		"""
		Increment ball position, assuming no collisions.
		"""

		self.position = add(self.position, self.velocity)
		self.canvas_.move(self.ball_,self.velocity[0], self.velocity[1])

		self.ballymax = self.position[1] + self.balldiameter/2.0
		self.ballymin = self.position[1] - self.balldiameter/2.0

		self.ballleftx = self.position[0] - self.balldiameter/2.0
		self.ballrightx = self.position[0] + self.balldiameter/2.0
		# #self.canvas_.update()

	def reset(self):
		"""Reset the position, velocity and number of hits of the ball"""

		self.position = [float(self.canvas_['width'])/2.0, float(self.canvas_['height'])/2.0]
		self.canvas_.coords(self.ball_,self.position[0] - self.balldiameter/2.0 ,
			self.position[1] - self.balldiameter/2.0)
		self.Nhits = 0

		self.velocity = (rd.choice([-1,1])*4,rd.choice([-1,1])*4)

class Paddle(object):
	def __init__(self,root,_canvas,_paddle):
		"""
		Create a paddle with the given inital position. 
		Assign methods to it
		Bind Keys
		"""

		self.paddle_ = _paddle
		self.canvas_ = _canvas

		self.currframe = 0

		self.direction_right = 0
		self.direction_left = 0

		self.paddlew = 30
		self.paddleh = 150

		self.position_ymin = [0,0] 
		self.position_ymax = [0,0]

		self.position_xout = [30,1070]

		tol = 10
 
		def upright(event):
			self.direction_right = -tol

		def downright(event):
			self.direction_right = +tol

		def upleft(event):
			self.direction_left = -tol

		def downleft(event):
			self.direction_left = +tol

		def onkeyrelease(event):
			key = event.keysym
			
			if (str(key)=="Up" or str(key)=="Down"):
				self.direction_right = 0

			if (str(key)=="w" or str(key)=="s"):
				self.direction_left = 0

		root.bind("<Up>",upright)
		root.bind("<Down>",downright)
		root.bind("<w>",upleft)
		root.bind("<s>",downleft)
		root.bind('<KeyRelease>', onkeyrelease)

	def left_paddle_constraint(self):

		"""prevent left paddle from moving out of screen"""

		return not ((self.canvas_.coords(self.paddle_[0])[1] < 0 
			and self.direction_left<0) 
			or (self.canvas_.coords(self.paddle_[0])[1] + self.paddleh > 600
			and self.direction_left>0))

	def right_paddle_constraint(self):

		"""prevent right paddle from moving out of screen"""

		return not ((self.canvas_.coords(self.paddle_[1])[1] < 0 
			and self.direction_right<0)
			or (self.canvas_.coords(self.paddle_[1])[1] + self.paddleh > 600
			and self.direction_right>0))

	def move(self):

		"""move paddle"""

		if (self.left_paddle_constraint()):
			self.canvas_.move(self.paddle_[0],0, self.direction_left)

		if (self.right_paddle_constraint()):
			self.canvas_.move(self.paddle_[1],0, self.direction_right)

		self.position_ymin[0] = self.canvas_.coords(self.paddle_[0])[1]
		self.position_ymax[0] = self.position_ymin[0] + self.paddleh
		
		self.position_ymin[1] = self.canvas_.coords(self.paddle_[1])[1]
		self.position_ymax[1] = self.position_ymin[1] + self.paddleh

	def do_animation(self):
		def do_image():
			self.canvas_.itemconfig(self.paddle_[0], image=self.paddle_[2][self.currframe])
			self.canvas_.itemconfig(self.paddle_[1], image=self.paddle_[3][self.currframe])
			
		try:
				do_image()
		except IndexError:
				# End of image list reached, start over at the first image 
				self.currframe = 0
				do_image()
		self.canvas_.update_idletasks() #Force redraw
		self.currframe = self.currframe + 1
		# Call myself again to keep the animation running in a loop
		
class Score(object):
	def __init__(self,canvas_,score_):
		"""
		Track the score
		"""
		self.score = score_
		self.canvas = canvas_

	def addPoint(self,player):
		currentscr = int(self.canvas.itemcget(self.score[player], 'text'))
		#self.score[player].set(currentscr + 1)
		self.canvas.itemconfig(self.score[player], text=str(currentscr+1))


	def reset(self):
		self.score[0].set(0)
		self.score[1].set(0)
	
class Pong(object):

	"""main routine"""
	"""create instances of the other class and create callbacks"""

	def __init__(self,root,_canvas,_ball,_paddle,_score,_cloud,_creature):

		self.ballc = Ball(_canvas,_ball)
		self.paddlec = Paddle( root,_canvas,_paddle)
		self.scorec = Score(_canvas,_score)

		self.brickwall = obs.Brickwall(_canvas)
		self.chaotic_cloud = obs.Chaotic_cloud(_canvas,_cloud)
		self.creaturec = obs.Creature(_canvas,_creature,self.ballc)
		self.gravity_field = gf.gravityfield(_canvas)
		self.portal = po.Portals(_canvas)

		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])

		self.stepcount = 0

			# 1 - Portal
			# 2 - Brickwall
			# 3 - Creature
			# 4 - Chaotic Cloud
			# 5 - Gravity Field

		self.obstacle_choice_init = [1,2,3,4,5]
		self.obstacle_choice = self.obstacle_choice_init[:]
		
		#either of these events could be defined with a lambda, but I 
		#wanted to show you how to refer to methods without calling them
		self.events = [self.hits_wall,
						self.hits_paddle,
						self.win,
						self.hits_brick_wall,
						self.hits_portal,
						self.inside_cloud,
						self.hits_monster]

		# each event function has a corresponding response function
		# a response function is a callback function
		self.responses = [lambda: self.ballc.reflect((0,self.direction),'wall'),
						lambda: self.ballc.reflect((self.direction,0),'paddle'), 
						lambda: self.scorec.addPoint(self.winner),
						lambda: self.ballc.reflect((self.direction,0),'brickwall'),
						lambda: self.ballc.hits_portal(self.teleportto),
						lambda: self.ballc.random_vel(),
						lambda: self.ballc.reflect((self.direction,0),'creature')]

	def hits_wall(self):

		upper_wall = self.ballc.position[1] < self.ballc.balldiameter/2.0

		lower_wall = self.ballc.position[1] > self.cheight - self.ballc.balldiameter/2.0

		if (upper_wall):

			self.direction = 1

		elif (lower_wall):

			self.direction = -1

		return (upper_wall or lower_wall)

	def hits_paddle(self):
		
		hits_left_paddle = (self.ballc.ballymax>self.paddlec.position_ymin[0] 
			and self.ballc.ballymin<self.paddlec.position_ymax[0] 
			and self.ballc.ballleftx < self.paddlec.position_xout[0])

		hits_right_paddle = (self.ballc.ballymax> self.paddlec.position_ymin[1] 
			and self.ballc.ballymin<self.paddlec.position_ymax[1] 
			and self.ballc.ballrightx > self.paddlec.position_xout[1])

		if (hits_left_paddle):

			self.direction = 1
		
		elif(hits_right_paddle):

			self.direction = -1

		if(hits_left_paddle or hits_right_paddle):

			if self.obstacle_choice == []:
				self.obstacle_choice = self.obstacle_choice_init[:]

			thechoice = rd.choice(self.obstacle_choice)
			self.obstacle_choice.remove(thechoice)

			if (thechoice == 1):

				if len(self.portal.portal) == 0:
					self.portal.create_portal()
				else:
					self.portal.delete_portal()

			if (thechoice == 2):

				if self.brickwall.n_bricks == 0:
					self.brickwall.create_bricks()

			if (thechoice == 3):

				if self.creaturec.creature == []:
					self.creaturec.create_creature()

			if (thechoice == 4):

				if self.chaotic_cloud.chaoticcloud == []:
					self.chaotic_cloud.create_cloud()

			if (thechoice == 5):

				if len(self.gravity_field.gravf) == 0:
					self.gravity_field.create_field()
				else:
					self.gravity_field.delete_field()

		return (hits_left_paddle or hits_right_paddle)

	def hits_brick_wall(self):

		if self.brickwall.n_bricks == 0:

			from_left = from_right = 0

		else: 

			from_left = (self.ballc.position[0] + self.ballc.balldiameter/2.0 > self.brickwall.x_brick 
				and self.ballc.position[0] + self.ballc.balldiameter/2.0 < self.brickwall.x_brick + self.brickwall.brick_width
				and self.brickwall.hit_brick_num(self.ballc.position[1]) != -1)

			from_right = (self.ballc.position[0] - self.ballc.balldiameter/2.0 < self.brickwall.x_brick + self.brickwall.brick_width
				and self.ballc.position[0] - self.ballc.balldiameter/2.0 > self.brickwall.x_brick
				and self.brickwall.hit_brick_num(self.ballc.position[1]) != -1)

			if (from_left):

				self.direction = -1

			if (from_right):

				self.direction = 1

			if (from_left or from_right):
				self.brickwall.delete_brick(self.ballc.position[1])

		return (from_left or from_right)

	def hits_portal(self):

		portal1=0
		portal2=0

		if len(self.portal.portal)!=0:

			yrange1 = [self.portal.portal_coords[self.portal.portalnum[0]][1], 
			self.portal.portal_coords[self.portal.portalnum[0]][3]]
			yrange1.sort()

			yrange2 = [self.portal.portal_coords[self.portal.portalnum[1]][1], 
			self.portal.portal_coords[self.portal.portalnum[1]][3]]
			yrange2.sort()

			portal1 = ((self.ballc.ballrightx>self.portal.portal_coords[self.portal.portalnum[0]][0]) 
				and (self.ballc.ballleftx<self.portal.portal_coords[self.portal.portalnum[0]][2]) 
				and (yrange1[0]<self.ballc.ballymax<yrange1[1] or yrange1[0]<self.ballc.ballymin<yrange1[1]))

			portal2 = ((self.ballc.ballrightx>self.portal.portal_coords[self.portal.portalnum[1]][0]) 
				and (self.ballc.ballleftx<self.portal.portal_coords[self.portal.portalnum[1]][2]) 
				and (yrange2[0]<self.ballc.ballymax<yrange2[1] or yrange2[0]<self.ballc.ballymin<yrange2[1]))

			if portal1:

				self.teleportto = [self.portal.portal_coords[self.portal.portalnum[1]][0] -
				(self.portal.portal_coords[self.portal.portalnum[0]][0] - self.ballc.position[0]),
				self.portal.portal_yin[self.portal.portalnum[1]]]
		
			if portal2:
				self.teleportto = [self.portal.portal_coords[self.portal.portalnum[0]][0] - 
				(self.portal.portal_coords[self.portal.portalnum[1]][0] - self.ballc.position[0]),
				self.portal.portal_yin[self.portal.portalnum[0]]]

			if (portal1 or portal2):

				if self.portal.portal_yin[self.portal.portalnum[0]] == self.portal.portal_yin[self.portal.portalnum[1]] ==27 :

					self.ballc.reflect((0,-1),'portal')

				if self.portal.portal_yin[self.portal.portalnum[0]] == self.portal.portal_yin[self.portal.portalnum[1]] == 572 :

					self.ballc.reflect((0,1),'portal')

		return(portal1 or portal2)

	def inside_cloud(self):

		insidecloud = 0

		if  self.chaotic_cloud.chaoticcloud != []:

			insidecloud = (self.chaotic_cloud.container_coords[0] < self.ballc.ballleftx
			and self.ballc.ballrightx <  self.chaotic_cloud.container_coords[2]
			and self.chaotic_cloud.container_coords[1] < self.ballc.ballymin
			and self.ballc.ballymax <  self.chaotic_cloud.container_coords[3])

			if insidecloud:	

				self.chaotic_cloud.container_coords = [0,0,0,0]

				self.chaotic_cloud.canvas.after(2000,self.chaotic_cloud.delete_cloud())

		return insidecloud


	def hits_monster(self):

		bump_from_right = bump_from_left = 0

		if (self.creaturec.creature != [] and self.creaturec.canvas.coords(self.creaturec.creature)[0] > 0):

			bump_from_right =  (self.ballc.ballrightx > self.creaturec.canvas.coords(self.creaturec.creature)[0]
				and self.creaturec.sign ==1)

			bump_from_left = (self.ballc.ballleftx < self.creaturec.canvas.coords(self.creaturec.creature)[0]
				and self.creaturec.sign ==-1)

			if bump_from_right:

				self.direction = 1
				self.creaturec.delete_creature()

			if bump_from_left:

				self.direction = -1
				self.creaturec.delete_creature()

		return (bump_from_right or bump_from_left)

	def win(self):

		left_win = ((self.ballc.ballymax< self.paddlec.position_ymin[1] 
			or self.ballc.ballymin>self.paddlec.position_ymax[1])
			and self.ballc.ballrightx > 1100 - self.paddlec.paddlew + 50)

		right_win = ((self.ballc.ballymax<self.paddlec.position_ymin[0] 
			or self.ballc.ballymin>self.paddlec.position_ymax[0])
			and self.ballc.ballleftx < 0)

		if (left_win == 1):

			self.winner = 0
			self.ballc.reset()

		elif(right_win == 1):

			self.winner = 1
			self.ballc.reset()

		if (left_win or right_win) and self.brickwall.n_bricks != 0:
			self.brickwall.delete_brick_wall()

		if (left_win or right_win) and len(self.gravity_field.gravf) != 0:
			self.gravity_field.delete_field()

		return (left_win or right_win)


	def step(self):
		"""
		Calculate the next game state.
		"""
		self.stepcount = self.stepcount + 1

		self.ballc.move()
		self.paddlec.move()
		if (self.stepcount == 5):
			self.paddlec.do_animation()
			self.stepcount = 0

		if (len(self.gravity_field.gravf) != 0):
			self.gravity_field.rotate()
			bbox = self.gravity_field.canvas_.coords(self.gravity_field.gravf[3])
			self.ballc.position = [self.ballc.position[0],bbox[1]]

			self.ballc.canvas_.coords(self.ballc.ball_,self.ballc.position[0] - self.ballc.balldiameter/2.0 ,
			self.ballc.position[1] - self.ballc.balldiameter/2.0)

		# check for events
		if (self.creaturec.creature != []):
			self.creaturec.follow_ball()
		
		for event, response in zip(self.events, self.responses):
			if event():
				response()

	def run(self):
		"""
		For the game loop.
		""" 
		self.step()
		
		


		

			
