
import math as m
import time
import numpy as np

class gravityfield(object):

	def __init__(self,_canvas):

		self.radius = [50,100,150,230]
		self.canvas_ = _canvas
		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.colors = ['green','blue','red','yellow']
		self.thetastep = [0,0,0,0]
		self.gravf = []

	def poly_oval(self,x0,y0, x1,y1, steps=20, rotation=0):
	#"""return an oval as coordinates suitable for create_polygon"""

		# x0,y0,x1,y1 are as create_oval

		# rotation is in degrees anti-clockwise, convert to radians
		rotation = rotation * math.pi / 180.0

		# major and minor axes
		a = (x1 - x0) / 2.0
		b = (y1 - y0) / 2.0

		# center
		xc = x0 + a
		yc = y0 + b

		point_list = []

		# create the oval as a list of points
		for i in range(steps):

			# Calculate the angle for this step
			# 360 degrees == 2 pi radians
			theta = (math.pi * 2) * (float(i) / steps)

			x1 = a * math.m.cos(theta)
			y1 = b * math.m.sin(theta)

			# rotate x, y
			x = (x1 * math.m.cos(rotation)) + (y1 * math.m.sin(rotation))
			y = (y1 * math.m.cos(rotation)) - (x1 * math.m.sin(rotation))

			point_list.append(round(x + xc))
			point_list.append(round(y + yc))

		return point_list

	def create_field(self):

		self.upedge = self.canvas_.create_rectangle(30,53,1070,55,fill = "yellow",dash = (5, 1, 2, 1))
		self.downedge = self.canvas_.create_rectangle(30,545,1070,547,fill = "yellow",dash = (5, 1, 2, 1))

		for i in range(4):

			dict = {}
			dict['outline'] = self.colors[i]
			dict['fill']   = ''
			dict['smooth'] = 'true'
			dict['dash'] = (5, 1, 2, 1)
			dict['width'] = 10

			self.gravf.append(self.canvas_.create_polygon(tuple(self.poly_oval(self.cwidth/2.0 - self.radius[i],
				self.cheight/2.0 - self.radius[i],
				self.cwidth/2.0  + self.radius[i],
				self.cheight/2.0 + self.radius[i])),dict))

	def delete_field(self):

		for i in range(4):

			self.canvas_.delete(self.gravf[i])

		self.canvas_.delete(self.upedge)
		self.canvas_.delete(self.downedge)
		self.gravf = []

	def poly_oval(self,x0,y0, x1,y1, steps=20, rotation=0):
   # """return an oval as coordinates suitable for create_polygon"""

	# x0,y0,x1,y1 are as create_oval

		# rotation is in degrees anti-clockwise, convert to radians
		rotation = rotation * m.pi / 180.0

		# major and minor axes
		a = (x1 - x0) / 2.0
		b = (y1 - y0) / 2.0

		# center
		xc = x0 + a
		yc = y0 + b

		point_list = []

		# create the oval as a list of points
		for i in range(steps):

			# Calculate the angle for this step
			# 360 degrees == 2 pi radians
			theta = (m.pi * 2) * (float(i) / steps)

			x1 = a * m.cos(theta)
			y1 = b * m.sin(theta)

			# rotate x, y
			x = (x1 * m.cos(rotation)) + (y1 * m.sin(rotation))
			y = (y1 * m.cos(rotation)) - (x1 * m.sin(rotation))

			point_list.append(round(x + xc))
			point_list.append(round(y + yc))

		return point_list

	def rotate(self):

		self.thetastep[0] = self.thetastep[0] - 1/5.0
		self.thetastep[1] = self.thetastep[1] + 2/5.0
		self.thetastep[2] = self.thetastep[2] - 4/5.0
		self.thetastep[3] = self.thetastep[3] + 8

		for i in range(4):

			self.canvas_.coords(self.gravf[i],tuple(self.poly_oval(self.cwidth/2.0 - self.radius[i],
					self.cheight/2.0 - self.radius[i],
					self.cwidth/2.0  + self.radius[i],
					self.cheight/2.0 + self.radius[i], rotation=self.thetastep[i])))



