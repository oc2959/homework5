import random as rd

class Portals(object):

	def __init__(self,_canvas):

		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.canvas_ = _canvas
		self.portal_coords = [[30,0,self.cwidth/2.0 - 70,7],
								[self.cwidth/2.0 + 70,0,self.cwidth - 30,7],
								[30,self.cheight ,self.cwidth/2.0 - 70,self.cheight - 8 ],
								[self.cwidth/2.0 + 70, self.cheight ,self.cwidth - 30, self.cheight -8]]
		self.portal_yin = [7 + 20,7 + 20,self.cheight - 8 - 20,self.cheight -8 - 20]

		self.portal = []

	def randomize(self):

		self.portalnum = [0,1,2,3]
		self.portal1 = rd.choice(self.portalnum)
		self.portalnum.remove(self.portal1)
		self.portal2 = rd.choice(self.portalnum)

		self.portalnum = [self.portal1,self.portal2]

	def create_portal(self):

		self.randomize()

		for i in range(2):
			
			self.portal.append(self.canvas_.create_rectangle(self.portal_coords[self.portalnum[i]], fill = 'green'))


	def delete_portal(self):

		for i in range(2):
			
			self.canvas_.delete(self.portal[i])
			

		self.portal = []
			


	