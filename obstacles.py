import random as rd

class Brickwall(object):

	def __init__(self,_canvas):

		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.canvas_ = _canvas

		self.n_bricks = 0

	def create_bricks(self):

		self.brick = []
		self.init_n_bricks = self.n_bricks = rd.choice([2,3,4,5])
		self.brick_height = float(self.cheight)/self.n_bricks
		self.brick_width = 30
		self.x_brick = self.cwidth/2.0 - self.brick_width/2.0
		self.y_brick = []

		for i in range(self.n_bricks):

			self.brick.append(self.canvas_.create_rectangle(self.x_brick ,i*self.brick_height ,self.x_brick + self.brick_width,
				i*self.brick_height + self.brick_height,fill='orange', outline = 'white'))
			self.y_brick.append([i*self.brick_height, i*self.brick_height + self.brick_height])

	def hit_brick_num(self,a):
		# find index of self.y_brick that contains a
		index = -1

		for i in range(len(self.y_brick)):
			if a >= self.y_brick[i][0] and a <= self.y_brick[i][1]:
				index = i
				break

		return index

	def delete_brick(self,ycoord):

		num = self.hit_brick_num(ycoord)
		self.canvas_.delete(self.brick[num])
		self.y_brick[num] = [-1,-1]
		self.n_bricks = self.n_bricks - 1

	def delete_brick_wall(self):

		for i in range(self.init_n_bricks):

			self.canvas_.delete(self.brick[i])

		self.n_bricks = 0


class Creature(object):

	def __init__(self,_canvas,_creatureimg,_ball):

		self.canvas = _canvas
		self.img = _creatureimg
		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.w = _creatureimg.width()
		self.h = _creatureimg.height()
		self.creature = []
		self.ball = _ball 
		self.hitball = 0

	def create_creature(self):

		self.right_or_left = rd.choice([-500,500])
		self.sign = self.right_or_left/abs(self.right_or_left)
		anchor = ''

		if (self.sign == 1):
			anchor = 'nw'
		else:
			anchor = 'ne'
			
		self.creature = self.canvas.create_image(self.ball.position[0] + self.right_or_left, self.ball.position[1] - self.h/2.0, image=self.img, anchor=anchor)

	def follow_ball(self):

		#self.monster_x = self.monster_x + sign()
		self.hitball = self.hitball - self.sign * 1
		self.canvas.coords(self.creature, self.ball.position[0] + self.right_or_left + self.hitball, self.ball.position[1] - self.h/2.0)

	def delete_creature(self):

		self.canvas.delete(self.creature)
		self.creature = []

class Chaotic_cloud(object):

	def __init__(self,_canvas,chaoticcloudimg):

		self.canvas = _canvas
		self.img = chaoticcloudimg
		self.cheight = float(_canvas['height'])
		self.cwidth = float(_canvas['width'])
		self.w = chaoticcloudimg.width()
		self.h = chaoticcloudimg.height()

		self.chaoticcloud = []

	def create_cloud(self):

		if(rd.choice([1,2]) == 1):


			self.chaoticcloud = self.canvas.create_image(50, (self.cheight - self.h)/2.0, image=self.img, anchor='nw')
			self.container_coords = [100, (self.cheight - self.h)/2.0 +100, 6 + self.w , 
				(self.cheight - self.h)/2.0+ self.h - 50]

		else:

			self.chaoticcloud = self.canvas.create_image(self.cwidth-self.w-50, (self.cheight - self.h)/2.0, image=self.img, anchor='nw')
			self.container_coords = [self.cwidth-self.w, (self.cheight - self.h)/2.0 +100, self.cwidth/2.0 + self.w - 15 , 
			(self.cheight - self.h)/2.0+ self.h - 50]

	def delete_cloud(self):

		self.canvas.delete(self.chaoticcloud)
		self.chaoticcloud = []







	